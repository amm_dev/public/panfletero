const pubs = require('../Services/PublishingServices');
const tools = require('../Services/CommonServices');
const cfg = tools.getConfig().broadcast_settings;

module.exports = {

    /**
     * Configura e inicia o ciclo principal de postagens
     * @param {Array} list uma lista de entidades do tipo Source
     */
    start(list) {

        let size = list.length;
        let random = tools.getConfig().random_start;
        let index = random ? Math.floor(Math.random() * size) : 0;

        if (cfg.rounds <= 0) throw 'Número de postagens por ciclo precisa ser maior que 0.';
        if (cfg.delay <= 1) throw 'Período de pausa entre ciclos precisa ser maior que 1s.';

        let job = pubs.task(cfg.rule, async () => {

            (function postingCycle(i) {
                setTimeout(async () => {
                    if (index == size) index = 0;
                    if (list[index].content.length == 0) try {
                        list[index] = await pubs.recharge(list[index]);
                    } catch (e) {
                        throw e;
                    }
    
                    let items = list[index].content.length;
                    let medias = list[index].getArticle();
        
                    tools.log('>> Panfletando ' + list[index].name + ' (restam ' + (items-1) + ' artigos)');
        
                    for (let media of medias) {
                        await tools.ping();
                        await pubs.post(media);
                    }
                    index++;
                    if (i--) postingCycle(i);
                }, cfg.delay * 1000)
            })(cfg.rounds-1);
        })

        job();
        return;

    }

 }