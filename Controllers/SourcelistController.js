const tools = require('../Services/CommonServices');
const srcs = require('../Services/SourceServices');
const Source = require('../Entities/Source');

module.exports = {

    init() {

        let list = [];

        try {
            list = srcs.load();
        } catch(e) {
            throw 'SourcelistController (start): ' + e;
        }

        if (list.length > 0) {
            tools.log('Lista de fontes: ' + list.length + ' itens.');
            return list;
        } else {
            tools.log('A lista não foi carregada.');
            return;
        }

    },

    async call(srcRef) {

        if (typeof srcRef != 'object') throw srcRef;

        let { name, feed, profile, order, targets } = srcRef;
        if (!name || !feed || !profile || !order || !targets) throw 'SourcelistController (call): Fonte falta dados.'
        
        let start = Date.now();
        try {
            await tools.ping();
            let data = await srcs.get(feed);
            let time = ((Date.now() - start)/1000).toFixed(2);

            // CREATE OBJECT "SOURCE"
            let srcObj = new Source(name, feed, profile, order, targets, data.items);
            tools.log('>> "' + srcObj.name + '" respondeu em ' + time 
                + 's com ' + srcObj.content.length + ' artigos!'
                + (profile != 'default' ? ' (Perfil: ' + profile + ')' : ''))

            return srcObj;
        } catch(e) { 
            let time = ((Date.now() - start)/1000).toFixed(2);
            tools.log('>> "' + name + '" retornou ' + e + ' (' + time + 's)')
            return;
        }

    }

}