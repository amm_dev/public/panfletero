const fs = require('fs');
const onlineCheck = require('internet-available');
const config = require(process.env.PANFLETERO_CFG + '/botconfig.json');

module.exports = {

    log(msg) {
        let logText = new Date().toLocaleString() + ' - ' + msg;
        let logFile = process.env.PANFLETERO_CFG + config.log_path + new Date().toDateString() + '.log';

        console.log(logText);

        fs.appendFileSync(logFile, logText + '\r\n', 'utf8', function (error) {
            if (error) console.error( new Date().toLocaleString() + ' (erro) - ' + error);
        });

    },

    async ping(retry = true) {
        await onlineCheck({timeout: 7000, retries: 3 })
            .then(() => {
                return true;
            })
            .catch(async () => {
                if (retry === true) {
                    this.log('Offline, aguardando conexão...')
                    await this.ping();
                } else {
                    this.log('Offline, encerrado...')
                    return false;
                }
            });
    },

    getConfig() {
        return config;
    }

}
