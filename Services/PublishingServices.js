const mastodonAPI = require('mastodon-api');
const twitterAPI = require('twitter');
const nodeSchedule = require('node-schedule');
const tools = require('./CommonServices');
const srcs = require('./SourceServices');

const cfg = tools.getConfig();

 module.exports = {

    mastodon: new mastodonAPI({
        client_key: cfg.mastodon_env.client_key,
        client_secret: cfg.mastodon_env.client_secret,
        access_token: cfg.mastodon_env.access_token,
        timeout_ms: 5000,
        api_url: cfg.mastodon_env.instance + 'api/v1/'
    }),

    twitter: new twitterAPI({
        consumer_key: cfg.twitter_env.consumer_key,
        consumer_secret: cfg.twitter_env.consumer_secret,
        access_token_key: cfg.twitter_env.access_token_key,
        access_token_secret: cfg.twitter_env.access_token_secret
    }),

    async post(article) {

        switch (article.target) {
            case 'mastodon':
                await this.mastodon.post('statuses', article.params, (error,data) => {
                    if (error) tools.log('    ! Erro ao postar para ' + article.target + ': ' + JSON.stringify(error));
                    else tools.log('    * Postagem enviada para ' + article.target + ': ID ' + data.id);
                });
                break;
            case 'twitter':
                await this.twitter.post('statuses/update', article.params, (error,data) => {
                    if (error) tools.log('    ! Erro ao postar para ' + article.target + ': ' + JSON.stringify(error));
                    else tools.log('    * Postagem enviada para ' + article.target + ': ID ' + data.id);
                });
                break;
            default:
                throw 'Alvo de publicação inválido.';
        }
        
    },

    task(cron,job) {
        return () => {
            let t = nodeSchedule.scheduleJob(cron,job);
        }
    },

    async recharge(source) { 

        tools.log('>> Recarregando "' + source.name + '":');
        
        let start = Date.now();
        try {
            let data = await srcs.get(source.feed);

            let time = ((Date.now() - start)/1000).toFixed(2);
            source.content = data.items;

            let prevLoad = source.lastLoad;
            source.lastLoad = Date.now();
            source.sortNews();
            source.profileApply();
            
            tools.log('    * Recarregou com ' + source.content.length + ' artigos em ' + time + 's.');
            tools.log('    * Recarga anterior: ' + prevLoad.toLocaleString());

            return source;
        } catch (e) {
            let time = ((Date.now() - start)/1000).toFixed(2);
            throw '>> "' + source.name + '" retornou ' + e + ' (' + time + 's)';
        }
    }

 }