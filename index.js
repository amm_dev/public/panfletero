process.env['PANFLETERO_CFG'] = __dirname;

const sourcelist = require('./Controllers/SourcelistController');
const tools = require('./Services/CommonServices');
const broadcast = require('./Controllers/PublishingController');
const cfg = tools.getConfig();

(async () => {

    tools.log('[ PANFLETERO INICIOU! ]')
    
    tools.log('Checando conexão, por favor aguarde...')
    await tools.ping();                                     // Se offline, aguarda conexão
    // if (!await tools.ping(false)) process.exit(1);       // Se offline, encerra o bot
    tools.log('Online!');

    let feedList = [];
    let sources = sourcelist.init();

    if (!sources || !Array.isArray(sources)) {
        if (sources) tools.log(sources);
        else tools.log('Nada foi carregado! Verifique possíveis problemas!')
    }

    for (let source of sources) {
        await sourcelist.call(source).then(feed => {
            if (feed) feedList.push(feed);
        }).catch((e) => {
            tools.log('Erro ao carregar artigos de ' + source.name + ': ' + e);
            //process.exit(1);
        });
    }
    
    let size = feedList.length;
    tools.log('Fonte(s) online: ' + size + (size > 1 ? ' itens.' : ' item.'));

    tools.log('[ === PANFLETANDO! === ]')

    try {
        broadcast.start(feedList);
    } catch (e) {
        tools.log('Erro no ciclo de postagens: ' + e);
        //process.exit(1);
    }

})();

// Caso o bot seja encerrado por CTRL + C ou outro sinal de interrupção
process.on('SIGINT', () => {
    tools.log("[ Execução cancelada pelo operador! ]");
    process.exit(0);
});

