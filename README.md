**Leitor RSS em node.js.**
Distribui conteúdo para algumas redes sociais (Mastodon e Twitter).
Replica uma lista de fontes de jornais e blogs jornalísticos de interesse público popular.
***

## Requisitos
- Testado APENAS em ambiente Linux (Mint 19; Raspbian 10 Buster)
- node.js => 10 (exceto 12)
- Conexão com a Internet
- Acesso aos recursos de API das redes que se deseja replicar
***

## Uso
1. git clone "master"
2. Crie um arquivo "botconfig.json" a partir do "botconfig_model.json" com suas chaves de acesso
3.      npm install && node index.js
**Atenção**: o bot pode gravar arquivos de log, tenha algum espaço em disco e permissão de gravação.
***

#### Configuração

O arquivo "botconfig.json" passa os parâmetros para iniciar o bot.
- `log_path`: caminho para gravar os arquivos de log de atividade;
- `random_start`: se TRUE, inicia o ciclo de uma fonte aleatória da lista ao invés da primeira;
- `broadcast_settings`:
  -  `rule`: período das execuções, em estilo [CronTab](https://crontab-generator.org/);
  -  `rounds`: quantidade de postagens feitas por execução;
  -  `delay`: intervalo entre cada postagem da execução (em segs).

O restante das configurações serão específicas de cada API a receber postagens.  
***

#### Fontes
O arquivo "sources.json" passa os dados das fontes RSS para alimentar as postagens.
Cada bloco do arquivo cria uma entidade "Source" no carregamento do bot.
- `name`: o nome/rótulo da Fonte
- `feed`: a URI direta do arquivo XML contendo o feed RSS
- `profile`: implementado em `Source.profileApply()` para aplicar configurações personalizadas para a fonte;
- `order`: implementado em `Source.sortNews()` para definir a ordem em que os artigos serão organizados em `content`;
- `targets`: implementado em `Source.getArticle()` e controla a lista de alvos a se publicar items de `content`;
***

#### Demonstrações em Produção
- Mastodon: https://newsbots.eu/@leftwinghub_br
- Twitter: https://twitter.com/PanfleteroBot

***
#### Manifesto "Panfletero":
> 1. Propõe-se divulgar conteúdo jornalístico, audiovisual, dissertativo e/ou literário que seja de natureza diversa do produzido e controlado atualmente por um oligopólio de grupos de comunicação nacional, mas que mantenham um compromisso com a produção responsável e consciente deste conteúdo, priorizando o interesse público de um Brasil real, cercado de desigualdades, de acesso a informação concentrado e dificuldado e de debates prejudicados e contaminados pelo reacionarismo;
> 2. A referência autoral de todo conteúdo divulgado é explicitamente referenciada de maneira inequívoca. O propósito do projeto é o de penetração do conteúdo, estabelecendo uma ponte de ligação entre a fonte e as diversas bolhas informacionais formadas no interior das redes sociais, sejam elas contidas na web superficial ou privativa;
> 3. As fontes atualmente listadas foram selecinadas pelo curador do projeto, que considera necessário contribuir com a divulgação desses trabalhos independentes cujo rigor financeiro impede de concorrer em vias de igualdade com os grandes grupos tradicionais. A atualização dessa lista é feita por pesquisa conduzida individualmente e por sugestão pública, nos meios divulgados pelos perfis publicadores. As fontes não são apens de conteúdo jornalístico: produções artísticas e análises particulares também são consideradas fonte de informação, desde que claramente classificadas para serem consumidas como tal;
> 4. Consideram-se critérios fundamentais para seleção, a postura objetiva e clara das fontes como oposição a:
>  - Grupos de disseminação de ódio e preconceitos;
>  - Desmontes de direitos ou minimização/inviabilização de pautas da luta popular;
>  - Cerceamento de grupos socialmente vulneráveis (minorias);
>  - Promoção de ideologia capitalista predatória, insustentável ou danosa;
>  - Ataques anti-democráticos ou qualquer tipo de violência ou discriminação que firam os direitos humanos.
> 5. É observada também a origem financeira das fontes, dando preferência àquelas cuja parte minoritária ou nula venha de capital rentista ou de fundos internacionais, independente dos propósitos humanitários propagandeados pelos mesmos, pois considera-se que tal influência interfere negativamente na pauta e linha editorial, gerando viés de interesse e afetando a qualidade do debate público. Justifica-se tal postura na premissa de que o interesse privado se manifesta em ação política unicamente no objetivo de multiplicação e acúmulo de capital, e portanto, o envolvimento desse capital no trabalho científico das humanidades é uma forma de diversificar suas metologias para seguir intangível ao escrutínio público, mas mantendo a influência fisiológica e ideológica na sociedade, controlando as agendas de suas lutas, cooptando-as e nulificando seus esforços no médio e longo prazo.


**ATENÇÃO, AUTORES DE CONTEÚDO:** Em observação ao item 2 do Manifesto, caso você seja autor, editor ou curador/responsável por algum conteúdo indexado nesta lista e deseja que o mesmo seja **removido**, por favor me envie um e-mail com uma nota sobre a autoria. Irei verificar seu o caso o mais breve possível.
***

#### Créditos
Todos que contribuirem com o projeto.
Não há fim lucrativo.
Copyleft 2021.
